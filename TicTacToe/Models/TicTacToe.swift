//
//  TicTacToe.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 24/05/2021.
//

import Foundation
import SwiftUI

// MARK: - TicTacToe
struct TicTacToe {
    // MARK: private variables
    private let victoryPatterns: Set<Set<Int>> = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ]
    
    // MARK: open variables
    var grid: [Move?]
    let first: Player
    let second: Player
    
    // MARK: Compute Value
    var isDraw: Bool {
        grid.filter({ $0 == nil }).isEmpty
    }
    
    // MARK: Init
    init(first: Player, second: Player) {
        self.grid = Array(repeating: nil, count: 9)
        self.first = first
        self.second = second
    }
    
    // MARK: Open
    mutating func makeAMoveFor(_ player: PlayerType, at gridIndex: Int) {
        let currentPlayer = getPlayerFrom(player)
        makeAMoveFor(currentPlayer, at: gridIndex)
    }
    
    mutating func reset() {
        self.grid = Array(repeating: nil, count: 9)
    }
    
    func checkIsGridAlreadyPlayed(_ index: Int) -> Bool {
        !grid.filter { move -> Bool in
            guard let move = move else {
                return false
            }
            return move.gridIndex == index
        }.isEmpty
    }
    
    func checkWinner() -> Player? {
        if checkWinStateFor(first) {
            return first
        }
        
        if checkWinStateFor(second) {
            return second
        }
        
        return nil
    }
    
    mutating func makeAMoveIfOtherPlayerIsComputer(current player: PlayerType) {
        let nextPlayer = player == .first ? getPlayerFrom(.second) : getPlayerFrom(.first)
        guard nextPlayer.type == .computer else {
            return
        }
           
        // If you can win so WIN that game
        if let winIndex = getWinIndexFor(nextPlayer),
            !checkIsGridAlreadyPlayed(winIndex) {
            makeAMoveFor(nextPlayer, at: winIndex)
            return
        }
        
        // If you can block the other so block
        let adversary = getPlayerFrom(player)
        if let winIndex = getWinIndexFor(adversary),
           !checkIsGridAlreadyPlayed(winIndex) {
            makeAMoveFor(nextPlayer, at: winIndex)
            return
        }
        
        // Pick the center case
        let centerGridIndex = 4
        if !checkIsGridAlreadyPlayed(centerGridIndex) {
            makeAMoveFor(nextPlayer, at: centerGridIndex)
            return
        }
        // Check if is about to win
        // Make A random Move
        makeAMoveFor(nextPlayer, at: getRandomAvailableGrid())
    }
    
    // MARK: private Helpers
    mutating private func makeAMoveFor(_ player: Player, at gridIndex: Int) {
        guard !checkIsGridAlreadyPlayed(gridIndex) else {
            return
        }
        grid[gridIndex] = Move(player: player, gridIndex: gridIndex)
    }
    
    private func getPlayerFrom(_ type: PlayerType) -> Player {
        switch type {
        case .first:
            return first
        case .second:
            return second
        }
    }
    
    private func checkWinStateFor(_ player: Player) -> Bool {
        let playerMoves = grid.compactMap({ $0 }).filter({ $0.player == player })
        guard playerMoves.count > 2 else {
            return false
        }
        
        let player1MovesSet = Set<Int>(playerMoves.map({ $0.gridIndex }))
        for victoryPartern in victoryPatterns {
            if victoryPartern.isSubset(of: player1MovesSet) {
                return true
            }
        }
        
        return false
    }
    
    private func getRandomAvailableGrid() -> Int {
        let availableIndexes = grid.enumerated().map({ $0.element == nil ? $0.offset : nil }).compactMap({ $0 })
        return availableIndexes[Int.random(in: 0..<availableIndexes.count)]
    }
    
    private func getWinIndexFor(_ player: Player) -> Int? {
        let playerIndexes = Set(grid.filter({ $0?.player == player }).compactMap({ $0?.gridIndex }))
        
        for victoryPattern in victoryPatterns {
            let subtractingIndexes = victoryPattern.subtracting(playerIndexes)
            if let winIndex = subtractingIndexes.first,
               subtractingIndexes.count == 1 && !checkIsGridAlreadyPlayed(winIndex) {
                return winIndex
            }
        }
        
        return nil
    }
}

// MARK: - Nested Value
extension TicTacToe {
    enum PlayerType {
        case first, second
    }
    
    struct Move {
        let player: Player
        let gridIndex: Int
    }
}
