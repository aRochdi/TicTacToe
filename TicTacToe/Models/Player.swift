//
//  Player.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 20/06/2021.
//

import Foundation

struct Player: Equatable {
    let name: String
    let type: `Type`
    
    enum `Type` {
        case human, computer
    }
    
    static func == (lhs: Player, rhs: Player) -> Bool {
        lhs.name == rhs.name && lhs.type == rhs.type
    }
    
    static func buildPlayer(name: String) -> Player {
        Player(name: name, type: .human)
    }
    
    static func buildComputer() -> Player {
        Player(name: "computer", type: .computer)
    }
}
