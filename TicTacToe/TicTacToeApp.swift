//
//  TicTacToeApp.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 24/05/2021.
//

import SwiftUI

@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            GameView()
        }
    }
}
