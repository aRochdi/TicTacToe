//
//  AlertItem.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 17/06/2021.
//

import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let buttonTitle: Text
}

struct AlertContext {
    static let drawAlert = AlertItem(title: Text("Draw"), message: Text("That was a hell of game"), buttonTitle: Text("Retry"))
    
    static func getWinAlertFor(player: String) -> AlertItem {
        AlertItem(title: Text("Win"),
                  message: Text("The --\(player)-- is the Winner and Can you redo it ?"),
                  buttonTitle: Text("Retry"))
    }
}
