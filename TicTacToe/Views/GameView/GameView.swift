//
//  GameView.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 24/05/2021.
//

import SwiftUI

struct GameView: View {
//    @StateObject var viewModel = GameViewModel(firstPlayerName: "Miss", secondPlayerName: "Mister")
    @StateObject var viewModel = GameViewModel(playerName: "Mister")
    @State var alertItem: AlertItem?
    
    var body: some View {
        VStack {
            Text(viewModel.time)
                .font(.largeTitle)
                .bold()
            
            HStack {
                Text(viewModel.firstPlayer)
                    .foregroundColor(viewModel.firstPlayerColor)
                    .font(.title)
                    .bold()
                
                Spacer()
                
                Text(viewModel.secondPlayer)
                    .foregroundColor(viewModel.secondPlayerColor)
                    .font(.title)
                    .bold()
            }
            .padding()
                        
            LazyVGrid(columns: viewModel.gridItems, content: {
                ForEach(0..<9) { index in
                    ZStack {
                        Rectangle()
                            .foregroundColor(.blue)
                            .frame(width: 100, height: 100)
                        
                        if let aCase = viewModel.moves[index] {
                            ImageCase(systemName: aCase.systemImageName,
                                      tintColor: aCase.tintColor)
                        }
                    }
                    .cornerRadius(8)
                    .onTapGesture {
                        self.didTapCaseFor(index)
                        self.checkGameStatus()
                        
                    }
                    .disabled(isCaseEnabled(index))

                }
            })
            .padding()
            Spacer()
        }
        .padding(.top, 40)
        .alert(item: $alertItem, content: { alertItem in
            Alert(title: alertItem.title,
                  message: alertItem.message,
                  dismissButton: Alert.Button.cancel(alertItem.buttonTitle, action: {
                self.viewModel.resetGame()
                self.makeAMoveIfOnePlayerIsAComputer()
            }))
        })
        .onAppear {
            self.viewModel.startTimer()
            self.makeAMoveIfOnePlayerIsAComputer()
        }
    }
    
    private func isCaseEnabled(_ index: Int) -> Bool {
        self.viewModel.iCaseAvailable(with: index)
    }
    
    private func didTapCaseFor(_ index: Int) {
        self.viewModel.makeAMoveFor(index)
    }
    
    private func checkGameStatus() {
        switch self.viewModel.checkGameStatus() {
        case .win(let player):
            alertItem = AlertContext.getWinAlertFor(player: player)
        case .draw:
            alertItem = AlertContext.drawAlert
        case .stillOn:
            viewModel.nextTurn()
            makeAMoveIfOnePlayerIsAComputer()
        }
    }
    
    private func makeAMoveIfOnePlayerIsAComputer() {
        if viewModel.makeAMoveIfOtherPlayerIsComputer() {
            checkGameStatus()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
    }
}

struct ImageCase: View {
    let systemName: String
    let tintColor: Color
    
    var body: some View {
        Image(systemName: systemName)
            .resizable()
            .foregroundColor(tintColor)
            .frame(width: 50, height: 50)
    }
}
