//
//  TTTGameViewModel.swift
//  TicTacToe
//
//  Created by Rochdi Aries on 06/06/2021.
//

import SwiftUI

class GameViewModel: ObservableObject {
    // MARK: Public Variable
    let gridItems = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var time: String {
        let minutes = getFormatedTime(gameTime / 60)
        let seconds = getFormatedTime(gameTime % 60)
        return "\(minutes):\(seconds)"
    }
    
    var moves: [Move?] {
        tictactoe.grid.map({ move in
            guard let move = move else {
                return nil
            }
            
            let turn = getPlayerTurnFor(move.player)
            return Move(playerTurn: turn, index: move.gridIndex)
        })
    }
    
    var firstPlayer: String {
        tictactoe.first.name
    }
    
    var secondPlayer: String {
        tictactoe.second.name
    }
    
    var firstPlayerColor: Color {
        PlayerTurnType.first.tintColor
    }
    
    var secondPlayerColor: Color {
        PlayerTurnType.second.tintColor
    }
    
    // MARK: Private Variable
    @Published private var tictactoe: TicTacToe
    private var timer: Timer!
    private var currentPlayerTurn: PlayerTurnType = .first
    private var gameTime: Int
    
    init(firstPlayerName: String, secondPlayerName: String, gameTime: Int = 0) {
        self.tictactoe = TicTacToe(first: .buildPlayer(name: firstPlayerName), second: .buildPlayer(name: secondPlayerName))
        self.gameTime = gameTime
    }
    
    init(playerName: String, gameTime: Int = 0) {
        self.tictactoe = TicTacToe(first: .buildPlayer(name: playerName), second: .buildComputer())
        self.gameTime = gameTime
    }
     
    // MARK: Public Functions
    func iCaseAvailable(with index: Int) -> Bool {
        tictactoe.checkIsGridAlreadyPlayed(index)
    }
    
    func makeAMoveFor(_ index: Int) {
        let playerType = getPlayerFor(currentPlayerTurn)
        self.tictactoe.makeAMoveFor(playerType, at: index)
    }
    
    func makeAMoveIfOtherPlayerIsComputer() -> Bool {
        guard let computer = [tictactoe.first, tictactoe.second].first(where: { $0.type == .computer }) else {
            return false
        }
        
        let computerTurn = getPlayerTurnFor(computer)

        if currentPlayerTurn == computerTurn {
            let adversaryTurn = getAdversaryFor(computerTurn)
            tictactoe.makeAMoveIfOtherPlayerIsComputer(current: getPlayerFor(adversaryTurn))
            return true
        }
        
        return false
    }
    
    func checkGameStatus() -> GameStatus {
        if let winner = tictactoe.checkWinner() {
            return .win(player: winner.name)
        }
        
        if tictactoe.isDraw {
            return .draw
        }
                
        return .stillOn
    }
    
    func nextTurn() {
        currentPlayerTurn = getAdversaryFor(currentPlayerTurn)
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
            self.updateTime()
        })
    }
    
    func invalidTimer() {
        timer?.invalidate()
    }
    
    func resetGame() {
        self.currentPlayerTurn = .first
        self.gameTime = 0
        self.startTimer()
        self.tictactoe.reset()
    }
    
    // MARK: Helpers
    private func getAdversaryFor(_ playerTurn: PlayerTurnType) -> PlayerTurnType {
        playerTurn == .first ? .second : .first
    }
    
    private func getFormatedTime(_ component: Int) -> String {
        component < 10 ? "0\(component)" : "\(component)"
    }
    
    private func getPlayerTurnFor(_ player: Player) -> PlayerTurnType {
        player == tictactoe.first ? .first : .second
    }
    
    private func getPlayerFor(_ playerTurn: PlayerTurnType) -> TicTacToe.PlayerType {
        switch playerTurn {
        case .first:
            return .first
        case .second:
            return .second
        }
    }
    
    @objc private func updateTime() {
        gameTime += 1
    }
    
    struct Move {
        let playerTurn: PlayerTurnType
        let index: Int
        
        var systemImageName: String {
            playerTurn.systemImageName
        }
        
        var tintColor: Color {
            playerTurn.tintColor
        }
    }
    
    enum PlayerTurnType {
        case first
        case second
        
        fileprivate var systemImageName: String {
            switch self {
            case .first:
                return "xmark"
            case .second:
                return "circle"
            }
        }
        
        fileprivate var tintColor: Color {
            switch self {
            case .first:
                return .green
            case .second:
                return .red
            }
        }
    }
    
    enum GameStatus {
        case win(player: String)
        case draw
        case stillOn
    }
}
