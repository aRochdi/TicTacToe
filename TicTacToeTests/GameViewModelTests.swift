//
//  GameViewModelTests.swift
//  TicTacToeTests
//
//  Created by Rochdi Aries on 07/06/2021.
//

@testable import TicTacToe
import XCTest

class GameViewModelTests: XCTestCase {
    
    func test_GameViewModel_Move_Player_1() {
        // GIVEN
        let playerTurn: GameViewModel.PlayerTurnType = .first
        // WHEN
        let move = GameViewModel.Move(playerTurn: playerTurn, index: 0)
        // THEN
        
        XCTAssertEqual(move.systemImageName, "xmark")
        XCTAssertEqual(move.tintColor, .green)
    }
    
    func test_GameViewModel_Move_Player_2() {
        // GIVEN
        let playerTurn: GameViewModel.PlayerTurnType = .second
        
        // WHEN
        let move = GameViewModel.Move(playerTurn: playerTurn, index: 0)
        
        // THEN
        XCTAssertEqual(move.systemImageName, "circle")
        XCTAssertEqual(move.tintColor, .red)
    }

    func test_init_gameViewModel() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        
        // WHEN
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        
        // THEN
        XCTAssertEqual(viewModel.firstPlayer, firstPlayerName)
        XCTAssertEqual(viewModel.firstPlayerColor, .green)
        XCTAssertEqual(viewModel.secondPlayer, secondPlayerName)
        XCTAssertEqual(viewModel.secondPlayerColor, .red)
        XCTAssertEqual(viewModel.time, "00:00")
        XCTAssertEqual(viewModel.moves.count, 9)
    }
    
    func test_init_game_timer_00_05() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
                
        // WHEN
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName, gameTime: 5)
        
        // THEN
        XCTAssertEqual(viewModel.time, "00:05")
    }
    
    func test_init_game_timer_05_00() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
                
        // WHEN
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName, gameTime: 300)
        
        // THEN
        XCTAssertEqual(viewModel.time, "05:00")
    }
    
    func test_init_timer_11_22() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
                
        // WHEN
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName, gameTime: 682)
        
        // THEN
        XCTAssertEqual(viewModel.time, "11:22")
    }
    
    func test_init_timer_start_invalid() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        
        let promise = expectation(description: #function)
        
        // WHEN
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        
        viewModel.startTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            viewModel.invalidTimer()
            
            // THEN
            XCTAssertEqual(viewModel.time, "00:02")
            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_make_one_move() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let index = 0
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        viewModel.makeAMoveFor(index)
        
        // THEN
        XCTAssertEqual(viewModel.moves[index]?.index, index)
        XCTAssertEqual(viewModel.moves[index]?.playerTurn, .first)
    }
    
    func test_make_two_move() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.makeAMoveFor(secondMoveIndex)
        
        // THEN
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.index, firstMoveIndex)
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.playerTurn, .first)
        
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.index, secondMoveIndex)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.playerTurn, .first)
    }
    
    func test_make_two_move_and_nextturn() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(secondMoveIndex)
        
        // THEN
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.index, firstMoveIndex)
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.playerTurn, .first)
        
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.index, secondMoveIndex)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.playerTurn, .second)
    }
    
    func test_make_three_move_and_nextturn() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1
        let thirdMoveIndex = 2

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(secondMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(thirdMoveIndex)

        // THEN
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.index, firstMoveIndex)
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.playerTurn, .first)
        
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.index, secondMoveIndex)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.playerTurn, .second)
        
        XCTAssertEqual(viewModel.moves[thirdMoveIndex]?.index, thirdMoveIndex)
        XCTAssertEqual(viewModel.moves[thirdMoveIndex]?.playerTurn, .first)
    }
    
    func test_make_two_move_nextturn_with_third_move_same_as_the_second() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1
        let thirdMoveIndex = 1

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(secondMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(thirdMoveIndex)

        // THEN
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.index, firstMoveIndex)
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.playerTurn, .first)
        
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.index, secondMoveIndex)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.playerTurn, .second)
        
        XCTAssertEqual(viewModel.moves[thirdMoveIndex]?.index, thirdMoveIndex)
        XCTAssertEqual(viewModel.moves[thirdMoveIndex]?.playerTurn, .second)
    }
    
    func test_make_a_computer_move() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1

        let viewModel = GameViewModel(playerName: firstPlayerName)

        // WHEN
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.nextTurn()
        let isComputerMoveDone = viewModel.makeAMoveIfOtherPlayerIsComputer()
        viewModel.nextTurn()
        viewModel.makeAMoveFor(secondMoveIndex)

        // THEN
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.index, firstMoveIndex)
        XCTAssertEqual(viewModel.moves[firstMoveIndex]?.playerTurn, .first)
        XCTAssertTrue(isComputerMoveDone)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.index, secondMoveIndex)
        XCTAssertEqual(viewModel.moves[secondMoveIndex]?.playerTurn, .first)
    }
    
    func test_make_a_computer_move_without_computer() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)

        // WHEN
        let isComputerMoveDone = viewModel.makeAMoveIfOtherPlayerIsComputer()

        // THEN
        XCTAssertEqual(viewModel.moves.filter({ $0 != nil }).count, 0)
        XCTAssertFalse(isComputerMoveDone)
    }
    
    func test_check_game_status_stillOn() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let firstMoveIndex = 0
        let secondMoveIndex = 1
        let thirdMoveIndex = 2

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        viewModel.makeAMoveFor(firstMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(secondMoveIndex)
        viewModel.nextTurn()
        viewModel.makeAMoveFor(thirdMoveIndex)
        
        // WHEN
        let gameStatus = viewModel.checkGameStatus()
        
        // THEN
        switch gameStatus {
        case .win, .draw:
            XCTFail("The game should not be a draw or win not enough move")
        case .stillOn:
            break
        }
    }
    
    func test_check_game_status_win_player1() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let indexesMove = [0, 8, 1, 5, 2]

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        indexesMove.forEach { indexesMove in
            viewModel.makeAMoveFor(indexesMove)
            viewModel.nextTurn()

        }
        
        // WHEN
        let gameStatus = viewModel.checkGameStatus()
        
        // THEN
        switch gameStatus {
        case .stillOn, .draw:
            XCTFail("The game should not be a draw or win not enough move")
        case .win(let player):
            XCTAssertEqual(player, firstPlayerName)
        }
    }
    
    func test_check_game_status_win_player2() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let indexesMove = [0, 4, 3, 6, 7, 8, 5, 2]

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        indexesMove.forEach { indexesMove in
            viewModel.makeAMoveFor(indexesMove)
            viewModel.nextTurn()

        }
        
        // WHEN
        let gameStatus = viewModel.checkGameStatus()
        
        // THEN
        switch gameStatus {
        case .stillOn, .draw:
            XCTFail("The game should not be a draw or win not enough move")
        case .win(let player):
            XCTAssertEqual(player, secondPlayerName)
        }
    }
    
    func test_check_game_status_draw() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let indexesMove = [0, 4, 3, 6, 7, 8, 5, 1, 2]

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        indexesMove.forEach { indexesMove in
            viewModel.makeAMoveFor(indexesMove)
            viewModel.nextTurn()

        }
        
        // WHEN
        let gameStatus = viewModel.checkGameStatus()
        
        // THEN
        switch gameStatus {
        case .stillOn, .win:
            XCTFail("The game should not be a draw or win not enough move")
        case .draw:
            break
        }
    }
    
    func test_reset_game_after_a_draw() {
        // GIVEN
        let firstPlayerName = "firstPlayerName"
        let secondPlayerName = "secondPlayerName"
        let indexesMove = [0, 4, 3, 6, 7, 8, 5, 1, 2]

        let viewModel = GameViewModel(firstPlayerName: firstPlayerName, secondPlayerName: secondPlayerName)
        indexesMove.forEach { indexesMove in
            viewModel.makeAMoveFor(indexesMove)
            viewModel.nextTurn()

        }
        
        // WHEN
        viewModel.invalidTimer()
        viewModel.resetGame()
        let gameStatus = viewModel.checkGameStatus()
        
        // THEN
        XCTAssertEqual(viewModel.time, "00:00")
        XCTAssertEqual(viewModel.moves.count, 9)
        XCTAssertEqual(viewModel.moves.filter({ $0 != nil }).count, 0)
        
        switch gameStatus {
        case .draw, .win:
            XCTFail("The game should not be a draw or win not enough move")
        case .stillOn:
            break
        }
    }
}
