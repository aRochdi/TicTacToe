//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by Rochdi Aries on 24/05/2021.
//

@testable import TicTacToe
import XCTest

class TicTacToeTests: XCTestCase {
    
    // MARK: TicTacToe
    func test_initialization_human_player() {
        // GIVEN
        let playerName = "player"
        
        // WHEN
        let player = Player.buildPlayer(name: playerName)
        
        // THEN
        XCTAssertEqual(player.name, playerName)
        XCTAssertEqual(player.type, .human)
    }
    
    func test_initialization_computer_player() {
        // GIVEN
        let computerName = "computer"
        
        // WHEN
        let player = Player.buildComputer()
        
        // THEN
        XCTAssertEqual(player.name, computerName)
        XCTAssertEqual(player.type, .computer)
    }
    
    func test_initialization_grid() {
        // GIVEN
        let first = Player.buildPlayer(name: "player")
        let second = Player.buildComputer()
        
        // WHEN
        let tictactoe = TicTacToe(first: first, second: second)

        // THEN
        XCTAssertEqual(tictactoe.grid.count, 9)
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 0)
        XCTAssertEqual(tictactoe.first, first)
        XCTAssertEqual(tictactoe.second, second)
    }
    
    // MARK: TicTacToe Human Vs Human
    func test_make_a_move() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildPlayer(name: "player2")
        let gridIndex = 1
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        // WHEN
        tictactoe.makeAMoveFor(.first, at: gridIndex)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.gridIndex, gridIndex)
    }
    
    func test_make_two_move() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let gridIndexPlayer1 = 1

        let player2 = Player.buildPlayer(name: "player2")
        let gridIndexPlayer2 = 2
        
        var tictactoe = TicTacToe(first: player1, second: player2)

        // WHEN
        tictactoe.makeAMoveFor(.first, at: gridIndexPlayer1)
        tictactoe.makeAMoveFor(.second, at: gridIndexPlayer2)

        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 2)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.gridIndex, gridIndexPlayer1)
        
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer2]?.player, player2)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer2]?.gridIndex, gridIndexPlayer2)
    }
    
    func test_make_a_move_aleady_done() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let gridIndexPlayer1 = 1
        
        let player2 = Player.buildPlayer(name: "player2")

        var tictactoe = TicTacToe(first: player1, second: player2)
        tictactoe.makeAMoveFor(.first, at: gridIndexPlayer1)
        
        // WHEN
        tictactoe.makeAMoveFor(.second, at: gridIndexPlayer1)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 1)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.gridIndex, gridIndexPlayer1)
    }
    
    func test_make_check_Is_Grid_Already_Played() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let gridIndexPlayer1: Int = 1
        
        let player2 = Player.buildPlayer(name: "player2")
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        tictactoe.makeAMoveFor(.first, at: gridIndexPlayer1)
        
        // WHEN
        let isMoveAleardyExists = tictactoe.checkIsGridAlreadyPlayed(gridIndexPlayer1)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 1)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndexPlayer1]?.gridIndex, gridIndexPlayer1)
        XCTAssertTrue(isMoveAleardyExists)
    }
    
    func test_tictactoe_draw() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player1Moves = [1, 2, 3, 4, 8]
        let player2 = Player.buildPlayer(name: "player2")
        let players2Moves = [0, 5, 6, 7]
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        player1Moves.forEach { index in
            tictactoe.makeAMoveFor(.first, at: index)
        }
        
        players2Moves.forEach { index in
            tictactoe.makeAMoveFor(.second, at: index)
        }

        // THEN
        XCTAssertTrue(tictactoe.isDraw)
    }
    
    func test_tictactoe_winner_player1() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player1Moves = [0, 1, 2]
        let player2 = Player.buildPlayer(name: "player2")
        let players2Moves = [8, 5]
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        player1Moves.forEach { index in
            tictactoe.makeAMoveFor(.first, at: index)
        }
        
        players2Moves.forEach { index in
            tictactoe.makeAMoveFor(.second, at: index)
        }
        
        let winner = tictactoe.checkWinner()

        // THEN
        XCTAssertEqual(winner, player1)
    }
    
    func test_tictactoe_winner_player2() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player1Moves = [0, 8, 1]
        let player2 = Player.buildPlayer(name: "player2")
        let players2Moves = [2, 4, 6]
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        player1Moves.forEach { index in
            tictactoe.makeAMoveFor(.first, at: index)
        }
        
        players2Moves.forEach { index in
            tictactoe.makeAMoveFor(.second, at: index)
        }
        
        let winner = tictactoe.checkWinner()

        // THEN
        XCTAssertEqual(winner, player2)
    }
    
    func test_tictactoe_checkWinner_no_more_than_2_moves() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player1Moves = [0, 1]
        let player2 = Player.buildPlayer(name: "player2")
        let players2Moves = [0, 5]
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        player1Moves.forEach { index in
            tictactoe.makeAMoveFor(.first, at: index)
        }
        
        players2Moves.forEach { index in
            tictactoe.makeAMoveFor(.second, at: index)
        }
        
        let noWinner = tictactoe.checkWinner()

        // THEN
        XCTAssertNil(noWinner)
    }
    
    func test_tictactoe_reset() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player1Moves = [1, 2, 3, 4, 8]
        let player2 = Player.buildPlayer(name: "player2")
        let players2Moves = [0, 5, 6, 7]
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        player1Moves.forEach { index in
            tictactoe.makeAMoveFor(.first, at: index)
        }
        
        players2Moves.forEach { index in
            tictactoe.makeAMoveFor(.second, at: index)
        }
        // WHEN
        tictactoe.reset()
        
        // THEN
        XCTAssertEqual(tictactoe.grid.count, 9)
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 0)
    }
    
    // MARK: TicTacToe Human Vs Computer
    func test_without_computer_makeAMoveIfOtherPlayerIsComputer() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildPlayer(name: "player2")
        let gridIndex = 1
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        tictactoe.makeAMoveFor(.first, at: gridIndex)

        // WHEN
        tictactoe.makeAMoveIfOtherPlayerIsComputer(current: .first)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.gridIndex, gridIndex)
    }
    
    func test_with_computer_make_a_move_except_4() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildComputer()
        let gridIndex = 1
        let centerCaseIndex = 4
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        tictactoe.makeAMoveFor(.first, at: gridIndex)
        tictactoe.makeAMoveIfOtherPlayerIsComputer(current: .first)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 2)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.gridIndex, gridIndex)
        XCTAssertEqual(tictactoe.grid[centerCaseIndex]?.player, player2)
        XCTAssertEqual(tictactoe.grid[centerCaseIndex]?.gridIndex, centerCaseIndex)
    }
    
    func test_with_computer_make_a_move_4() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildComputer()
        let gridIndex = 4
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        
        // WHEN
        tictactoe.makeAMoveFor(.first, at: gridIndex)
        tictactoe.makeAMoveIfOtherPlayerIsComputer(current: .first)
        
        // THEN
        XCTAssertEqual(tictactoe.grid.filter({ $0 != nil }).count, 2)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.player, player1)
        XCTAssertEqual(tictactoe.grid[gridIndex]?.gridIndex, gridIndex)
        XCTAssertEqual(tictactoe.grid.filter({ $0?.player.type == .computer }).count, 1)
    }
    
    func test_with_computer_get_winner_computer() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildComputer()
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        tictactoe.grid[0] = TicTacToe.Move(player: player1, gridIndex: 0)
        tictactoe.grid[1] = TicTacToe.Move(player: player1, gridIndex: 1)
        tictactoe.grid[4] = TicTacToe.Move(player: player2, gridIndex: 4)
        tictactoe.grid[6] = TicTacToe.Move(player: player2, gridIndex: 6)
        
        // WHEN
        tictactoe.makeAMoveIfOtherPlayerIsComputer(current: .first)

        let winner = tictactoe.checkWinner()
        
        // THEN
        XCTAssertEqual(winner, player2)
    }
    
    func test_with_computer_get_blocked_by_computer() {
        // GIVEN
        let player1 = Player.buildPlayer(name: "player1")
        let player2 = Player.buildComputer()
        
        var tictactoe = TicTacToe(first: player1, second: player2)
        tictactoe.grid[4] = TicTacToe.Move(player: player1, gridIndex: 4)
        tictactoe.grid[6] = TicTacToe.Move(player: player1, gridIndex: 6)
        tictactoe.grid[0] = TicTacToe.Move(player: player2, gridIndex: 0)
        tictactoe.grid[5] = TicTacToe.Move(player: player2, gridIndex: 5)
        
        // WHEN
        tictactoe.makeAMoveIfOtherPlayerIsComputer(current: .first)
        let winner = tictactoe.checkWinner()
        
        // THEN
        XCTAssertEqual(tictactoe.grid[2]?.player, player2)
        XCTAssertNil(winner)
    }
}
